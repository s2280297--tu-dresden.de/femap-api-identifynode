Sub Main()
    ' Try to get the Femap Application object
    On Error Resume Next
    Dim femapApp
    Set femapApp = GetObject(, "femap.model")

    If femapApp Is Nothing Then
        MsgBox "Failed to get Femap application object"
        Exit Sub
    End If
    On Error GoTo 0 ' Disable error handling

    ' Specify the node ID you want to identify
    ' Dim targetNodeID
    ' targetNodeID = 100 ' Replace with your node ID

	' Ask the user for the node ID
    Dim targetNodeID
    targetNodeID = InputBox("Please enter the node ID you want to identify:", "Node ID Input")

    ' Confirm Femap Application Initialization
    femapApp.feAppMessage(0, "Femap Application Initialized")

    ' Create a Node object
    Dim node
    Set node = femapApp.feNode

    If node Is Nothing Then
        MsgBox "Failed to create node object"
        Exit Sub
    End If

    ' Confirm Node Object Creation
    femapApp.feAppMessage(0, "Node object created")

    ' Find the node by ID
    Dim result
    result = node.Get(targetNodeID)

    ' Output the result of the Get method
    femapApp.feAppMessage(0, "Get method result: " & result)

    If result = -1 Then ' Result is -1 if the node is found
        femapApp.feAppMessage(0, "Node ID: " & targetNodeID & " Coordinates: (" & node.X & ", " & node.Y & ", " & node.Z & ")")

    Else
        femapApp.feAppMessage(0, "Node ID: " & targetNodeID & " not found.")
    End If

    ' Cleanup
    Set node = Nothing
    Set femapApp = Nothing
End Sub
