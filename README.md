# Femap API Program to identify the coordinates of a node by its ID

This small script written in  Visual Basic Application identifies the coordinates of a node by its ID. The script includes a lot of comments and some rows of code to debug. It has been tested by several models using the API Programming in Femap 2021.2. 